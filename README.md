# **raw2mlv** #
Transcode raw files into magic lantern MLV format thanks to Ilia3101 and his project [raw2mlv](https://github.com/ilia3101/LibMLV),
[forum_thread](https://www.magiclantern.fm/forum/index.php?topic=24386.0)


**raw2mlv**

![Screen_Shot_2017-07-26_at_10.05.56.png](https://i.postimg.cc/Vvnf8rHm/Screenshot-2019-11-11-at-22-22-26-png-scaled.png)

## HOWTO ##

- drag a raw file or multiple raw files onto raw2mlv.app. A menu will appear. Follow instructions

**regarding gatekeeper**

To supress gatekeeper hold ctrl button down(macOS Sierrra, Mojave, Catalina) while opening the application the first time. You can also change permissions from within privacy/security settings.

**Thanks to:** Ilia3101, a1ex, g3gg0
