#GNU public license

#This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 # 
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 # 
 # You should have received a copy of the GNU General Public License
 # along with this program; if not, write to the
 # Free Software Foundation, Inc,
 # 51 Franklin Street, Fifth Floor,
 # Boston, MA  02110-1301, USA.


#!/bin/bash
#changes size of terminal window
#tip from here http://apple.stackexchange.com/questions/33736/can-a-terminal-window-be-resized-with-a-terminal-command
#Will move terminal window to the left corner
#printf '\e[3;0;0t'
printf '\e[8;12;60t'
printf '\e[3;410;100t'

open -a Terminal

bold="$(tput bold)"
normal="$(tput sgr0)"
red="$(tput setaf 1)"
reset="$(tput sgr0)"
green="$(tput setaf 2)"
underline="$(tput smul)"
standout="$(tput smso)"
normal="$(tput sgr0)"
black="$(tput setaf 0)"
red="$(tput setaf 1)"
green="$(tput setaf 2)"
yellow="$(tput setaf 3)"
blue="$(tput setaf 4)"
magenta="$(tput setaf 5)"
cyan="$(tput setaf 6)"
white="$(tput setaf 7)"

btp=$(tput setaf 4)$(grep -Eo '.{0,0}-b.{0,3}' /tmp/bitdepth_raw2mlv)

while :
do 

    clear
    cat<<EOF
    =======	  
    ${bold}$(tput setaf 1)raw2mlv$(tput sgr0)
    -------
 
    $(tput bold)(01) create single MLV files$(tput sgr0)				 
    $(tput bold)(02) stitch into one MLV file$(tput sgr0) 
    $(tput bold)(03) specify bitdepth, 10/12/14/16 bit$(tput sgr0) $btp
    $(tput bold)$(tput setaf 1)(q)  Exit raw2mlv$(tput sgr0)

Please enter your selection number below:
EOF
    read -n2
    case "$REPLY" in

    "01")  
echo > /tmp/raw2mlv/raw2mlv_01 1> /dev/null 2>&1 &
rm /tmp/raw2mlv/raw2mlv 1> /dev/null 2>&1 &
osascript -e 'tell application "Terminal" to close first window' & exit
;;

    "02")  
echo > /tmp/raw2mlv/raw2mlv_02 1> /dev/null 2>&1 &
rm /tmp/raw2mlv/raw2mlv 1> /dev/null 2>&1 &
osascript -e 'tell application "Terminal" to close first window' & exit
;;

    "03")
if grep ' \-b' /tmp/bitdepth_raw2mlv 
then
rm /tmp/bitdepth_raw2mlv
clear
echo $(tput bold)"

$(tput sgr0)$(tput bold)$(tput setaf 1) 
bitdepth reset"$(tput sgr0) ; 
sleep 1
btp=
else
printf '\e[8;12;60t'
printf '\e[3;410;100t'
clear
echo $(tput bold)"Specify bitdepth:$(tput sgr0)(between$(tput sgr0) 1-16 and hit enter)"
read input_variable
echo "bitdepth is set to: $(tput bold)$(tput setaf 4)$input_variable"$(tput sgr0)
printf "%s\n" " -b $input_variable" >> /tmp/bitdepth_raw2mlv
btp=$(tput setaf 4)$(grep -Eo '.{0,0}-b.{0,3}' /tmp/bitdepth_raw2mlv)
fi
sleep 1 
printf '\e[8;12;60t'
printf '\e[3;410;100t'
;;


    "q")   
echo > /tmp/raw2mlv/raw2mlv_exit 1> /dev/null 2>&1 &
rm /tmp/raw2mlv/raw2mlv 1> /dev/null 2>&1 &
osascript -e 'tell application "Terminal" to close first window' & exit
;;

    "Q")  echo "case sensitive!!"   ;;
     * )  echo "invalid option"     ;;
    esac
    sleep 0.5
done


#Copyright Danne