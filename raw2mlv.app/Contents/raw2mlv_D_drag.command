#GNU public license

#This program is free software; you can redistribute it and/or
 # modify it under the terms of the GNU General Public License
 # as published by the Free Software Foundation; either version 2
 # of the License, or (at your option) any later version.
 # 
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 # 
 # You should have received a copy of the GNU General Public License
 # along with this program; if not, write to the
 # Free Software Foundation, Inc,
 # 51 Franklin Street, Fifth Floor,
 # Boston, MA  02110-1301, USA.

    btp=$(grep -Eo '.{0,0}-b.{0,3}' /tmp/bitdepth_raw2mlv)

    while grep -q 'CR2\|cr2\|DNG\|dng' /tmp/raw2mlv/draglist_04
    do
    four=$(grep 'CR2\|cr2\|DNG\|dng' /tmp/raw2mlv/draglist_04 | awk 'FNR == 1 {print}')
    echo "$(tail -n +2 /tmp/raw2mlv/draglist_04)" > /tmp/raw2mlv/draglist_04
    raw2mlv "$four" $btp -o "$(echo $four | cut -d "." -f1)".MLV
    done